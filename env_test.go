package main

import (
	"os"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestEnvTools(t *testing.T) {
	Convey("Test Getting ENV variables", t, func() {
		Convey("Test ENV variable that exists", func() {
			err := os.Setenv("TEST_EXISTING", "IM HERE")
			val := getenv("TEST_EXISTING", "NOT HERE")
			So(err, ShouldBeNil)
			So(val, ShouldNotBeEmpty)
			So(val, ShouldNotEqual, "NOT HERE")
			So(val, ShouldEqual, "IM HERE")
		})
		Convey("Test ENV variable that does not exist", func() {
			val := getenv("TEST_NONEXISTING", "NOT HERE")
			So(val, ShouldNotBeEmpty)
			So(val, ShouldEqual, "NOT HERE")
		})
	})
}
