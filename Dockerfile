FROM alpine:latest
LABEL "io.infradrift"="Infradrift Project"
LABEL "infradrift-system"="API Server"
LABEL "base-image"="alpine:latest"
LABEL "maintainer"="support@infradrift.io"

RUN apk add --no-cache ca-certificates

COPY infradriftd /bin/infradriftd

ENTRYPOINT ["infradriftd"]