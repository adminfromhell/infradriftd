# InfraShiftd
[![DepShield Badge](https://depshield.sonatype.org/badges/infrashift/infrashiftd/depshield.svg)](https://depshield.github.io)
  
[![CodeFactor](https://www.codefactor.io/repository/github/infradrift/infradriftd/badge)](https://www.codefactor.io/repository/github/infradrift/infradriftd)
  
[![CircleCI](https://circleci.com/gh/infradrift/infradriftd/tree/master.svg?style=svg)](https://circleci.com/gh/infradrift/infradriftd/tree/master)
  
[![codecov](https://codecov.io/gh/infradrift/infradriftd/branch/master/graph/badge.svg)](https://codecov.io/gh/infradrift/infradriftd)